import numpy as np
import matplotlib.pyplot as plt
from matplotlib.colors import LogNorm
import torch
import model_func as m
import matplotlib as mpl
import scipy.stats as stats

with_latex_style = {
    'font.family': 'stixgeneral',
    'mathtext.fontset': 'stix',
    "axes.labelsize": 28,
    "font.size": 30,
    "legend.fontsize": 24,
    "xtick.labelsize": 26,
    "ytick.labelsize": 26,
    "legend.fancybox": False,
    "lines.linewidth": 1.0,
    "patch.linewidth": 1.0,
    "savefig.bbox": "tight",
    "savefig.dpi": 150
    }


mpl.rcParams.update(with_latex_style)

evaluation_device = 'cuda' if torch.cuda.is_available() else 'cpu'

# Training & network parameters
cINN_kw = {'dim': 2,        # dimension of input x & output z (latents)
           'dim_c': 100,         # dimension of condition which is equivalent to the observables y
           'N_blocks': 6,       # number of multiplicative layer
           'coupling_layer_architecture': 'RNVP',  # architecture type of coupling layer, GLOW or RNVP e.g.
           'use_permutation': True,  # if a permutation layer after every multiplicative layer is used
           'end_to_end': True,                   # Whether to leave the cond. net fixed
           'exponent_clamping': 4.0,  # "soft clamping for the multiplicative component"
           'init_scale': 0.1,  # initialize model parameters from a normal distribution with this sigma
           'internal_size': 128,  # internal size of subnets for s,t mappings (hidden_layer_sizes)
           'verbose_construction': False  # if the network structure should be printed or not
           }

training_kw = {'lr_init': 1.0e-3,     # initial learning rate
               'batch_size': 100,   # batch size (number of data sets used per epoch)
               'n_epochs': 50,      # total number of epochs for training
               'n_its_per_epoch': 100,   # number of iterations per epoch
               'pre_low_lr': 0,  # use int for number of iterations with surpressed learning rate (*1e-1) before actual training epochs start
               'adam_betas': (0.9, 0.999),  # Adam optimizer beta parameters
               'cosine_scheduler': True,  # cosine scheduler for the learning rate, starting at lr_init & falling to final_lr
               'final_lr': 1e-5,
               'weight_decay': 1e-5   # L2 weight regularization of model parameters
               }

# Load model and training, set to evaluation mode
model_class = m.Model(cINN_kw, training_kw)
model = model_class.model
state_dicts = torch.load('test_model_gaussian.pt', map_location=evaluation_device)
model.load_state_dict(state_dicts['net'])
model.eval()

# Load test data
x_test = torch.tensor(np.load('x_test.npy'))
y_test = torch.tensor(np.load('y_test.npy'))

# Choose e.g.1 example test data set to get posteriors
index = 0
x = x_test[index]
y = y_test[index]


def sample_posterior(c, N_sampling=10000):
    """
    Sample posterior of one specific test set.
    """
    rev_inputs = torch.randn(int(N_sampling), cINN_kw['dim']).to(evaluation_device)
    rev_condition = torch.zeros(int(N_sampling), cINN_kw['dim_c']).to(evaluation_device)
    rev_condition += c

    with torch.no_grad():
        x_samples = model(rev_inputs, c=rev_condition, rev=True)
    outputs = x_samples.data.cpu().numpy()

    return outputs


def plot_loss(self):
    """
    Function that loads & plots the training loss & learning rate.
    The saved losses are the mean over the whole data set
    (smallest "dimension": batch size)
    """
    data = np.load('training_loss.npy')  # shape n_epochs, loss, learning rate
    training_loss, test_loss, lr = data[:, 0], data[:, 1], data[:, 2]

    epochs = np.arange(1, np.shape(training_loss)[0]+1)

    colors = ['skyblue', 'steelblue']

    print("First and last training loss", training_loss[0], training_loss[-1])
    print('Last validation loss', test_loss[-1])

    plt.figure(figsize=(12, 8))
    plt.plot(epochs, training_loss, linestyle='-',
             linewidth=2.5, color=colors[0])
    plt.xlabel(r'Epoch')
    plt.ylabel(rf'Loss')
    plt.grid(alpha=0.6)
    plt.savefig('training_loss.pdf')
    if np.all(np.array(training_loss) > 0):
        plt.semilogy()
        plt.savefig('training_loss_log.pdf')
        plt.close()
    else:
        plt.close()
        plt.figure(figsize=(12, 8))
        plt.plot(epochs, training_loss-np.min(training_loss), linestyle='-',
                 linewidth=2.5, color=colors[0])
        plt.xlabel(r'Epoch')
        plt.ylabel(rf'Loss - minimum loss')
        plt.semilogy()
        plt.grid(alpha=0.6)
        plt.savefig(F'training_loss_log.pdf')
        plt.close()

    if len(test_loss) > 0:
        plt.figure(figsize=(12, 8))
        plt.plot(epochs, test_loss, linestyle='-',
                 linewidth=2.5, color=colors[1])
        plt.xlabel(r'Epoch')
        plt.ylabel(rf'Loss')
        plt.grid(alpha=0.6)
        plt.savefig(F'test_loss.pdf')
        if np.all(np.array(test_loss) > 0):
            plt.semilogy()
            plt.savefig(F'output/loss/test_loss_log.pdf')
            plt.close()
        else:
            plt.close()
            plt.figure(figsize=(12, 8))
            plt.plot(epochs, test_loss-np.min(test_loss), linestyle='-',
                     linewidth=2.5, color=colors[1])
            plt.xlabel(r'Epoch')
            plt.ylabel(rf'Loss - minimum loss')
            plt.semilogy()
            plt.grid(alpha=0.6)
            plt.savefig(F'output/loss/test_loss_log.pdf')
            plt.close()

    plt.figure(figsize=(12, 8))
    plt.plot(epochs, lr, linestyle='-',
             linewidth=2.5, color=colors[1])
    plt.xlabel(r'epoch')
    plt.ylabel(rf'Log learning rate')
    plt.grid(alpha=0.6)
    plt.savefig(F'lr.pdf')


def plot_posteriors():
    """
    Function to plot posteriors
    """
    parameters = ['standard_deviation', 'mean']
    posterior_samples = sample_posterior(c=y)
    posterior_dict = {}
    for i, parameter in enumerate(parameters):
        posterior_dict[parameter] = posterior_samples[:, i]

    # 68% interval for sigma estimation:
    confidence = 0.68
    q_low = 100. * 0.5 * (1 - confidence)
    q_high = 100. * 0.5 * (1 + confidence)

    std = {}
    bins = {}
    center_of_bins = {}
    hist = {}
    for param in parameters:
        bins[param] = np.linspace(np.min(posterior_dict[param]), np.max(posterior_dict[param]), 51)
        center_of_bins[param] = (bins[param][:-1] + bins[param][1:])/2.
        hist[param] = np.histogram(posterior_dict[param], bins[param])
        std[param] = np.percentile(posterior_dict[param], [q_low, q_high])

    hist_2d = np.histogram2d(posterior_dict['standard_deviation'],
                             posterior_dict['mean'],
                             bins=(bins['standard_deviation'], bins['mean']))
    colormap_min = 0.0001
    colormap_max = 0.05

    fig, ax = plt.subplots(2, 2, figsize=(16, 16))
    for i, param in enumerate(parameters):
        ax[0, i].grid(alpha=0.4)
        ax[0, i].axvline(np.mean(posterior_dict[param]), color='k', label=r'$\langle \mathbf{\theta} \rangle $')
        ax[0, i].axvline(x[i].numpy(), color='r', label='MC truth')
        ax[0, i].bar(center_of_bins[param], hist[param][0]/np.sum(hist[param][0]),
                     width=(bins[param][1]-bins[param][0]), color='cornflowerblue',
                     edgecolor='cornflowerblue')
    cax = ax[1, 1].imshow(hist_2d[0]/np.sum(hist_2d[0]),
                          cmap='Greys', aspect='auto', origin='lower',
                          extent=[bins['mean'][0], bins['mean'][-1],
                                  bins['standard_deviation'][0], bins['standard_deviation'][-1]],
                          norm=LogNorm(vmin=colormap_min, vmax=colormap_max))

    ax[0, 0].set_ylabel(r'$P(\theta | MC)$')
    ax[0, 0].set_title(r'$\sigma$')
    ax[0, 1].set_title(r'$\mu$')
    ax[0, 0].set_xlabel(r'$\sigma$')
    ax[1, 1].set_ylabel(r'$\sigma$')
    ax[1, 1].set_xlabel(r'$\mu$')
    fig.delaxes(ax.flatten()[2])
    plt.subplots_adjust(left=None, bottom=None, right=0.94, top=None, wspace=0.1,
                        hspace=0.1)
    cb_ax = fig.add_axes([0.96, 0.11, 0.02, 0.77])
    cb = fig.colorbar(cax, cax=cb_ax)
    cb.set_label(r'$P(\theta | MC)$')  # , fontsize=24)
    text_string1 = r'$\langle \sigma \rangle = %.2f\ _{-%.2f} ^{+%.2f}$' % (np.mean(posterior_dict['standard_deviation']), abs(np.mean(posterior_dict['standard_deviation'])-std['standard_deviation'][1]), abs(np.mean(posterior_dict['standard_deviation'])-std['standard_deviation'][0]))
    text_string1 += '\n'
    text_string1 += r'$\langle \mu \rangle = %.2f\ _{-%.2f} ^{+%.2f}$' % (np.mean(posterior_dict['mean']), abs(np.mean(posterior_dict['mean'])-std['mean'][1]), abs(np.mean(posterior_dict['mean'])-std['mean'][0]))
    plt.figtext(0.1, 0.11, text_string1)
    plt.savefig(F'posteriors_{index}.pdf')

    return posterior_dict


def plot_observables(posterior_dict):
    """
    Plot of observables (= y test data), the underlying distribution (Gaussian distribution with x_test parameters)
    and Gaussian distribution using parameters from mean of posteriors
    """
    bins = np.linspace(np.min(y.numpy()), np.max(y.numpy()), 10)
    center_of_bins = (bins[:-1] + bins[1:])/2.
    hist = np.histogram(y.numpy(), bins=bins)
    x_values = np.linspace(-7, 7, 100)
    plt.figure(figsize=(12, 8))
    plt.bar(center_of_bins, hist[0],
            width=(bins[1]-bins[0]), color='cornflowerblue',
            edgecolor='cornflowerblue', label='Histrogrammed test data')
    plt.plot(x_values,
             np.max(hist[0])*stats.norm.pdf(x_values, x[1], x[0])/np.max(stats.norm.pdf(x_values, x[1], x[0])),
             '-', color='k', label=r'Gaussian distribution with $\mu =$ %.2f, $\sigma=$ %.2f' % (x[1], x[0]))
    plt.plot(x_values,
             np.max(hist[0])*stats.norm.pdf(x_values, np.mean(posterior_dict['mean']), np.mean(posterior_dict['standard_deviation']))/np.max(stats.norm.pdf(x_values, np.mean(posterior_dict['mean']), np.mean(posterior_dict['standard_deviation']))),
             '--', color='r', label=r'Fitted gaussian distribution with $\mu =$ %.2f, $\sigma=$ %.2f' % (np.mean(posterior_dict['mean']), np.mean(posterior_dict['standard_deviation'])))
    plt.legend()
    plt.savefig(F'observables_{index}.pdf')


posterior_dict = plot_posteriors()
plot_observables(posterior_dict)
