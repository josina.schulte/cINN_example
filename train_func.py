import torch
import torch.nn
import torch.optim
from torch.autograd import Variable
import numpy as np
import model_func as m


def cosine_scheduler(epoch, base_lr_orig, final_lr, n_epochs):
    max_update = int(n_epochs*2/3)
    if epoch <= max_update:
        base_lr = final_lr + (base_lr_orig - final_lr) * (1 + np.cos(np.pi * epoch / max_update)) / 2
    else:
        base_lr = final_lr
    return base_lr


def train(cINN_kw, training_kw, train_loader, test_loader, filename_out):
    """
    Function implementing the training procedure for cINN
    :param cINN_kw: dictionary containing parameters for network setup
    :param training_kw: dictionary containing training parameters
    :param train_loader: torch Loader with training data
    :param test_loader: torch Loader with test data
    """
    model_class = m.Model(cINN_kw, training_kw)
    model = model_class.model
    print('Starting training...')
    train_losses_total = []
    for i_epoch in range(-training_kw['pre_low_lr'], training_kw['n_epochs']):
        print(f"Epoch {i_epoch} / {training_kw['n_epochs']} starting now.")
        model.train()
        batch_idx = 0
        loss_history_iterations = []
        testloss_history_iterations = []

        if i_epoch < 0:
            for param_group in model_class.optim.param_groups:
                param_group['lr'] = training_kw['lr_init'] * 1e-1
        elif i_epoch == 0:
            print('First learning rate')
            for param_group in model_class.optim.param_groups:
                param_group['lr'] = training_kw['lr_init']
        elif training_kw['cosine_scheduler']:
            print("Using cosine learning rate scheduler.")
            c = cosine_scheduler(i_epoch, training_kw['lr_init'],
                                 training_kw['final_lr'],
                                 training_kw['n_epochs'])
            for param_group in model_class.optim.param_groups:
                param_group['lr'] = c
            print(f"In epoch {i_epoch}: learning rate = {c}")

        for x, y in train_loader:

            if batch_idx >= training_kw['n_its_per_epoch']:
                break

            batch_idx += 1
            x, y = Variable(x).to(model_class.device), Variable(y).to(model_class.device)
            (z, jac) = model(x, c=y)  # size: ( (training_kw['batch_size'], cINN_kw['dim']), (training_kw['batch_size']) )
            zz = torch.sum(z**2, dim=-1)  # size: (training_kw['batch_size'])

            neg_log_likeli = 0.5 * zz - jac  # size: (training_kw['batch_size'])
            loss = torch.mean(neg_log_likeli)  # size: 1
            loss.backward()

            model_class.optim_step()
            loss_history_iterations.append([loss.item(), 0., 0.])

        epoch_losses = np.mean(np.array(loss_history_iterations), axis=0)
        # print(epoch_losses)
        with torch.no_grad():
            print('Starting validation phase.')
            batch_idx = 0
            model.eval()
            for x, y in test_loader:
                if batch_idx >= training_kw['n_its_per_epoch']:
                    break

                batch_idx += 1

                x, y = Variable(x).to(model_class.device), Variable(y).to(model_class.device)
                (z, jac) = model(x, c=y)  # size: (training_kw['batch_size'], cINN_kw['dim'])
                zz = torch.sum(z**2, dim=-1)  # size: (training_kw['batch_size'])

                neg_log_likeli = 0.5 * zz - jac

                loss = torch.mean(neg_log_likeli)
                testloss_history_iterations.append(loss.item())

        epoch_losses[1] = np.mean(testloss_history_iterations)
        epoch_losses[2] = np.log10(model_class.optim.param_groups[0]['lr'])
        train_losses_total.append(epoch_losses)
    model_class.save(filename_out)
    np.save('training_loss.npy', train_losses_total)
    print(F'Saved at {filename_out}')
